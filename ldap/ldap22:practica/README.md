# ldap22 practica
## @edt  M06-ASIX

#### ldapserver 2022
* **ldap22:practica** Servidor ldap per implementar un schema personalitzat i supervisat per el servei phpldapadmin

## Parametres schema ldap
Crear un schema amb:

* Un nou objecte STRUCTURAL

* Un nou objecte AUXILIARU

* Cada objecte ha de tenir almenys 3 nous atributs.

* Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i   binary per contenir documents pdf.

* Crear una nova ou anomenada practica.

* Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass definits en l’schema. 

* Assegurar-se de omplir amb dades reals la foto i el pdf.

* Eliminar del slapd.conf tots els schema que no facin falta, deixar només els imprescindibles

* Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el pdf.




## Ordre per engegar el container
```
 docker run --rm   --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389  -d kevin16gonzalez/ldap22:schema 
```
## Creacio del schema
Dins del document empresaris.schema posem els seguents parametres

Un nou objecte STRUCTURAL
```
objectClass (1.1.2.2.1 NAME 'empresa'
  DESC ' treballadors de la empresa'
  SUP TOP
  STRUCTURAL
  MUST ( treballador $ alta $ contracte $ imatge ) )
```
Un nou objecte AUXILIARU
```
objectClass (1.1.2.2.2 NAME 'dadesGeneriques'
  DESC 'Informacio no rellevant del treballadors'
  SUP TOP
  AUXILIARY
  MUST ( curriculum $ habilitats $ casat)
  MAY nacionalitat )
```

Cada objecte ha de tenir almenys 3 nous atributs.

Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i   binary per contenir documents pdf.
### objectClass "empresa"
```
attributetype (1.1.2.1.1 NAME 'treballador'
  DESC 'nom del treballador'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )

attributetype (1.1.2.1.2 NAME 'alta'
  DESC 'alta true/false'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE )

attributetype (1.1.2.1.3 NAME 'contracte'
  DESC 'contracte en pdf'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
  SINGLE-VALUE )

attributetype (1.1.2.1.4 NAME 'imatge'
  DESC 'imatge del treballador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )
```

### objectClass "dadesGeneriques"
```
attributetype (1.1.2.1.5 NAME 'nacionalitat'
  DESC 'Indica el pais de origen'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )

attributetype (1.1.2.1.6 NAME 'curriculum'
  DESC 'el fitxer del curriculum en pdf'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
  SINGLE-VALUE )

attributetype (1.1.2.1.7 NAME 'habilitats'
  DESC 'Imatges que descriuen les habilitats del treballador '
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.1.8 NAME 'casat'
  DESC 'casat true/false'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE )
```
Crear una nova ou anomenada practica.

```
dn: ou=practica,dc=edt,dc=org
ou: practica
description: Container per a informatica sel sistema linux
objectclass: organizationalunit
```
Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass definits en l’schema. 
```
dn: treballador=juan,ou=practica,dc=edt,dc=org
ObjectClass: empresa
ObjectClass: dadesGeneriques
treballador: juan
alta: TRUE
contracte:< file:///opt/docker/contrato.pdf
imatge:<  file:///opt/docker/image1.jpeg
curriculum:< file:///opt/docker/curriculum.pdf
habilitats:< file:///opt/docker/habilidades.jpeg
casat: FALSE

dn: treballador=Lucas,ou=practica,dc=edt,dc=org
ObjectClass: empresa
treballador: Lucas
alta: TRUE
contracte:< file:///opt/docker/contrato.pdf
imatge:<  file:///opt/docker/image2.jpeg

dn: treballador=Pedro,ou=practica,dc=edt,dc=org
ObjectClass: empresa
ObjectClass: dadesGeneriques
treballador: Pedro
alta: TRUE
contracte:< file:///opt/docker/contrato.pdf
imatge:<  file:///opt/docker/image3.jpeg
curriculum:< file:///opt/docker/curriculum.pdf
habilitats:< file:///opt/docker/habilidades.jpeg
casat: FALSE
nacionalitat: Española
```
Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el pdf.

## Engegar el servei phpldapadmin
```
docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisx -p 80:80 -d kevin16gonzalez/phpldapadmin:base 
```
## Conexio grafica
En el browser
```
localhost/phpldapadmin
```
