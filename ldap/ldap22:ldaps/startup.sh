#!/bin/bash
cp  /opt/docker/ldap.conf /etc/ldap/ldap.conf
mkdir /etc/ldap/certs
cp /opt/docker/cacert.pem /etc/ldap/certs/
cp /opt/docker/serverkey.ldap.pem /etc/ldap/certs/
cp /opt/docker/servercert.ldap.pem  /etc/ldap/certs/
chmod 777 /etc/ldap/certs/serverkey.ldap.pem

echo "Inicialitzacio BD ldap edt.org"
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
/usr/sbin/slapd -d1  -h "ldap:/// ldaps:/// ldapi:///" && echo "slapd Ok"
