# ldap22 practica
## @edt  M06-ASIX

#### ldapserver 2022
* **ldap22:groups** Servidor ldap amb schemes indespensibles i amb grups creats amb el tipus posixGroup

*  1. Modificar les RDN dels usuaris per tal de que s’identifiquin en el seu DN per el seu
uid. Per exemple ‘cn=Pere Pou,ou=usuaris,dc=edt,dc=org’ passa a ser
‘uid=pere,ou=usuaris,dc=edt,dc=org’

*  2. Afegir una entitat organizationalUnit anomenada grups que servirà per contenir els
grups d’usuaris.

*  3. Crear cadascun dels grups amb entitats de tipus posixAccount. Podeu consultar
ldapwiki / phpldapadmin per observar l’estructura d’aquest objectClass. Cal crearà
els grups:
professors / 601 (no podem usar el GID 100 que és users)
alumnes / 600
1asix / 610
2asix / 611
wheel / 10
1wiam / 612
2wiam / 613
1hiaw / 614

Cal assignar cada un dels usuaris (els ‘normals’ i els userxx) al seu grup
corresponent segons el seu atribut gidNumber.
Assegurar-se de modificar els usuaris que tenen assignat el gidNumber a 100 i posar
el nou valor 601.

*  4. Eliminar de la configuració de slapd.conf els includes dels schema que no siguin
necessaris per la base de dades edt.org.

*  5. Pujeu la imatge al DockerHub, al Git i poseu-li la documentació/README
descriptiva.
## Ordre per engegar el container
```
 docker run --rm  --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389  -d kevin16gonzalez/ldap22:latest 
```


## Creacio d'un grup
Dins del fitxer edt-org.ldif posem els seguents parametres
```
dn: cn=professors,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: professors
gidNumber: 601
description: grup de professors
memberUid: pau 
memberUid: pere
memberUid: jordi

```
