# ldap22 base
## @edt  M06-ASIX

#### ldapserver 2022
* **ldap22:editat** Servidor ldap basic amb base de dades edt.org. Aquesta imatge te diverses modificacions en la structura del document edt-org.ldif i slapd.conf

Generar un sol fitxer ldif anomenat edt-org.ldif (usuaris0-11)


Afegir en el fitxer dos usuaris i una ou nova inventada i posar-los dins la nova ou.
```
dn: ou=informatica,dc=edt,dc=org
changetype: add
ou: informatica
description: Container per a informatica sel sistema linux
objectclass: organizationalunit

dn: uid=Pau Pou,ou=informatica,dc=edt,dc=org
changetype: add
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Pau Pou
sn: VG9udMOtc2ltbw==
homePhone: 694-200-2222
mail: Paupou@gmail.com
description: Watch out for this girl
ou: informatica
uid: Pau Pou
uidNumber: 69420
gidNumber: 600
homeDirectory: /tmp/home/Pau

dn: uid=Marta Pou,ou=informatica,dc=edt,dc=org
changetype: add
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Marta Pou
sn: VG9udMOtc2ltbw==
homePhone: 694-200-2223
mail: Martapou@gmail.com
description: Watch out for this girl
ou: informatica
uid: Marta Pou
uidNumber: 69420
gidNumber: 600
homeDirectory: /tmp/home/Marta

ldapmodify -x -D cn=Manager,dc=edt,dc=org -w secret -f mod.ldif 

```

Modificar el fitxer edt.org.ldif  modificant dn dels usuaris   utilitzant en lloc del cn el uid per identificar-los (rdn).
```
Es modifica directament el fitxer edt-org.ldif i es sustitueix manualment
ldapsearch -x -LLL -b 'dc=edt,dc=org' uid

dn: uid=Marta Mas,ou=usuaris,dc=edt,dc=org
uid: marta
uid: Marta Mas

dn: uid=Jordi Mas,ou=usuaris,dc=edt,dc=org
uid: jordi
uid: Jordi Mas

dn: uid=Admin System,ou=usuaris,dc=edt,dc=org
uid: admin
uid: Admin System

```
Configurar el password de Manager que sigui ‘secret’ però   encriptat (posar-hi un comentari per indicar quin és de cara a estudiar).
* Encriptem el passwd amb slappasswd amb el motor fred(fora del container)
```
docker exec -it ldap slappasswd -h{md5} -s secret
{MD5}Xr4ilOzQ4PCOq3aQ0qbuaQ==
```
Propagar el port amb -p -P
* Afegim la -p especificant el port 
```
docker run --rm --name ldap -h ldap --net 2hisx -p 389:389 -d kevin16gonzalez/ldap:editat
```
Verifiqueu que sou capaços de modificar la configuració de la base de dades edt.org en calent modificant per exemple el passwd o el nom o els permisos.
