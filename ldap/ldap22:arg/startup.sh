#!/bin/bash

if [ -f /var/lib/ldap/.ldap-data-created ]
then
	/usr/sbin/slapd -d0
else
	echo "Inicialitzacio BD ldap edt.org"
	rm -rf /var/lib/ldap/*
	rm -rf /etc/ldap/slapd.d/*
	slaptest -f slapd.conf -F /etc/ldap/slapd.d
	slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
	chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
	touch /var/lib/ldap/.ldap-data-created
	/usr/sbin/slapd -d0
fi
