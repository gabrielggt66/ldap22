# ldap22 practica
## @edt  M06-ASIX

#### ldapserver 2022
* **ldap22:arg** Servidor ldap que utilitza persistencia de dades a partir de la verificaio de la existencia de un fitxer creat la primera vegada que es munta la base de dades

## Ordre per engegar el container
```
 docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-conf:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d kevin16gonzalez/ldap22:arg

```
##Ldap22:arg
En el fitxer startup afegim les seguents linies de codi

```
if [ -f /var/lib/ldap/.ldap-data-created ]
then
	
	/usr/sbin/slapd -d0

else

echo "Inicialitzacio BD ldap edt.org"
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
touch /var/lib/ldap/.ldap-data-created

/usr/sbin/slapd -d0
fi

```

El primer cop la base dades s'inicializa de forma normal pero creant per primera vegada el fitxer .ldap-data-created a /var/lib/ldap.

```
else

echo "Inicialitzacio BD ldap edt.org"
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
touch /var/lib/ldap/.ldap-data-created

/usr/sbin/slapd -d0
fi

```
Amb les persistencia de dades al saber que el fitxer existeix ya no fa la contruccio de la base de dades si no que ho inicia ignorant el pas abasn esmentat.
```
if [ -f /var/lib/ldap/.ldap-data-created ]
then
	
	/usr/sbin/slapd -d0



